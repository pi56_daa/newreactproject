import React, {useState} from 'react';
import {Button, Container} from "react-bootstrap";
import CreateType from "../components/modals/CreateType";
import CreateDevice from "../components/modals/CreateDevice";


const Admin = () => {
    const [typeVisible, setTypeVisible] = useState(false)
    const [deviceVisible, setDeviceVisible] = useState(false)
    return (
        <Container className="d-flex flex-column">
            <Button onClick={() => setTypeVisible(true)} variant={"outline-dark"} className="mt-2 p-2">Добавить тип</Button>
            <Button onClick={() => setDeviceVisible(true)} variant={"outline-dark"} className="mt-2 p-2">Добавить товар</Button>
            <CreateType show={typeVisible} onHide={() => setTypeVisible(false)}/>
            <CreateDevice show={deviceVisible} onHide={() => setDeviceVisible(false)}/>
        </Container>
    );
};

export default Admin;