import React, {useState} from 'react';
import {Col, Container, Row, Button} from "react-bootstrap";
import BasketList from "../components/BasketList";
import CreateDelivery from "../components/modals/CreateDelivery";

const Basket = () => {
    const [deliveryVisible, setDeliveryVisible] = useState(false)
    return (
        <Container>
            <Row className="mt-2">
                <Col md={3}>
                    <BasketList />
                </Col>
                <Col md={3} className="ml-15">
                    <Button show={deliveryVisible} onClick={() => setDeliveryVisible(true)} variant={"outline-dark"} className="ml-15 mt-2 p-2">Добавить адресс</Button>
                    <CreateDelivery />
                </Col>
            </Row>
        </Container>
    );
};

export default Basket;