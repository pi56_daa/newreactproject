import React, {useState} from 'react';
import Modal from "react-bootstrap/Modal";
import  {Button, Form} from "react-bootstrap";
import {createDelivery} from "../../http/deviceAPI";
import {observer} from "mobx-react-lite";

const CreateDelivery = observer(({show, onHide}) => {
    const [value, setValue] = useState('')

    const addDelivery = () => {
        const delivery = {}
        delivery.value = value
        console.log(delivery);
        createDelivery({name: value}).then(data => {
            setValue('')
            onHide()
        })
    }

    return (
        <Modal
            show={show}
            onHide={onHide}
            size="lg"
            centered
        >
            <Modal.Header closeButton>
                <Modal.Title id="contained-modal-title-vcenter">
                    Добавить адресс
                </Modal.Title>
            </Modal.Header>
            <Modal.Body>
                <Form>
                    <Form.Control
                        value={value}
                        onChange={e => setValue(e.target.value)}
                        placeholder={"Введите аддрес"}
                    />
                </Form>
            </Modal.Body>
            <Modal.Footer>
                <Button variant="outline-danger" onClick={onHide}>Закрыть</Button>
                <Button variant="outline-success" onClick={addDelivery}>Добавить</Button>
            </Modal.Footer>
        </Modal>
    );
});

export default CreateDelivery;