import {Button, Card, Col, Container, Image, Row} from "react-bootstrap";
import bStar from "../assets/bStar.png";
import React from "react";
import {useHistory} from 'react-router-dom'
import DeviceStore from '../store/DeviceStore'
import {get} from "mobx";


const BasketDevice = () => {
    const device = {}
    device.id = document.cookie;
    const history = useHistory()
    return (
    <Container className="mt-3">
        <Row>
            <Col md={4}>
                <Image width={300} height={300} src={process.env.REACT_APP_API_URL + device.img}/>
            </Col>
            <Col md={4}>
                <Row className='d-flex flex-column align-items-center'>
                    <h2>{device.name}</h2>
                    <div
                        className='d-flex align-items-center justify-content-center'
                        style={{
                            background: `url(${bStar}) no-repeat center center`,
                            width: 120,
                            height: 120,
                            backgroundSize: 'cover',
                            fontSize: 32
                        }}
                    >
                        {device.rating}
                    </div>
                </Row>
            </Col>
            <Col md={4}>
                <Card
                    className="d-flex flex-column align-items-center justify-content-around"
                    style={{width: 150, height: 150, fontSize: 16, border: '5px solid lightgray'}}
                >
                    <h3>Цена: {device.price}</h3>
                </Card>
            </Col>
        </Row>
    </Container>
    )}

export default BasketDevice

